# Introduction to the laboratory and the programs we will be using


![main1.png](images/main1.png)

[verano2016 - Coralys]

The Introduction to Computer Programming course provides an introduction to the fundamental concepts of computer programming and the solution of problems through algorithm design, including parallel programming techniques. Students will have a full understanding of the basic components of a program starting with control structures using conditional and iterative instructions with logical expressions, as well as the definition and use of variables and data types. They will develop procedures and functions for common tasks that are repeated or more complex tasks that require decomposition, including recursive functions. They will use structured data types such as arrays, queues, files and pointers. Also, the students will be exposed to the computer's structure: input and output, memory, and the central processing unit. A general look into the Computer Science field and its effect in society, including ethical implications in computation, is included.

During this semester the students in the Introduction to Computer Programming course will be participating in the project: "Engaging Introductory Programming Laboratory" (EIP). In this project, sponsored by the TUES program of the National Science Foundation, students will be able to work on laboratory experiences where they will practice the concepts and skills learned in class and obtain attractive products that will motivate them with the possibilities that Computer Science offers them.

Each of the laboratory experiences will use libraries and programming sections that have been developed by professors and students of the Computer Science program of the University of Puerto Rico, RÃ­o Piedras campus. When the laboratory experience is completed, the student will not only reinforce the concepts learned in class, but will also get to know products that he or she will be capable of producing with a bit more knowledge and experience in computing. 

In today's laboratory experience you will learn how we will work in the upcoming meetings, how to access the files that will be used, and how to turn in the work. You will also learn to manage the basic elements of Qt, the platform that will allow us to develop and execute projects in C++.


## Objectives:

1) Understand the goals of the laboratory for the Introduction to Computer Programming course (CCOM-3033).

2) Get to know the laboratory experiences prepared and the general expectations of the laboratory sessions.

3) Get to know the programming in pairs strategy.

4) Get to know the rules of the laboratory sessions and the methods of evaluation.

5) Practice the use of *Git* and *Bitbucket* to download and store the necessary files for each laboratory experience.

6) Practice how to compile, fix errors, and execute a program using Qt.


---

---

## About the laboratory

### Course and evaluation policy

* All of the students should be registered in the Moodle page for Section 1 of the Laboratory using the email they most frequently read. The page can be found in moodle.ccom.uprrp.edu. All of the information, materials and assignments for the course will be on this page; each student is responsible for checking the page regularly and understanding the calendar for turning in their work.
* All cellphones must remain off during the laboratory session. If one of these devices interrupts the session, the professor will assume it's an emergency and will ask the student to leave the laboratory. Moreover, the cellphone must not be on vibrate or emit light signals, and it cannot be on the desk. Just shut it off and put it away. This way you may concentrate while in class.
* Only the laboratory laptops will be used.
* The laboratory session will start promptly, so you should arrive on time. In the case of something extraordinary happening in which the professor shall be absent or late, the Department will be notified. If you arrive more than 15 minutes late, please do not enter the laboratory. If you leave the laboratory, let it be for an emergency; it is expected that once the session starts, the student will remain in the laboratory until it is finished.
* For each laboratory session, there will be documents in Moodle related to the laboratory experience that you should read before arriving at the laboratory. Read and follow the instructions in the *Pre-Lab* session. The documents for each laboratory experience will be available in Moodle the Friday of the previous week.
* There will be a *Pre-Lab* quiz that you should take during the first 10 minutes of the laboratory session. The quiz is taken using Moodle and will not be available after 10 minutes from the time the laboratory session begins. The quiz will be related to the concepts learned in class that will be necessary for the laboratory experience, and will also include reading comprehension questions for the week's laboratory session instructions. Quizzes will not be reset or retaken. However, the worst grade will be eliminated.
* The laboratory experiences will be worked on in pairs. After each laboratory experience, each pair should hand in the files indicated in the instructions using Moodle. The final hand in date to hand in the files for each experience is the Tuesday that follows the laboratory session. Since the laboratory experiences will be worked on in pairs, only one member will hand in the work.
* When the work for each laboratory session is finished, each student must take a *Post-Test* which lasts 10 minutes. With the Pre-Lab and Post-Test we evaluate the utility of the laboratory experience in learning the concepts and skills. The Post-Test will not be worth any points in the laboratory grade, but will be used to give a bonus on each laboratory experience. The Post-Test will be available on Moodle on Friday when the laboratory session is held, until 11:59pm of the following Tuesday.
* Feel free to come by my office to clarify any doubts. If the door is open, you are welcome to come in; if it is closed, please wait for office hours. In the case of you having a scheduling conflict with the assigned office hours, you may schedule a meeting at any moment.
* Responsibility and honesty are very important qualities for the Computer Science Department. 


#### Evaluation

* The laboratory grade will be 25% of the course grade.
* The "Pre-Lab" tests will be 40% of the laboratory grade.
* The work turned in will be 60% of the laboratory grade.
* The criteria to obtain a bonus of 5 points in each laboratory experience are as follows:

    1)  You obtain 80 in the Pre-Lab and maintain or improve your grade in the Post-Test. 
    
    2)  You obtain 75 or more in the Pre-Lab and improve by 15 points or more from Pre-Lab to Post-Test.

    3)  You obtain 80 or more in the Pre-Lab and improve by 10 points or more from Pre-Lab to Post-Test.

    4)  You obtain 85 or more in the Post-Test.

* The total score for the delivered work will not be more than 100. 



### Programming in pairs

During the entire semester the laboratory experiences will be worked on using the programming in pairs technique. Each pair will use one computer and each student in the pair will have one of two roles: conductor or navigator. The conductor will be in charge of writing and editing the code or the requirement for the laboratory. The navigator will read and evaluate what the conductor is writing to indicate if there was an error or to make suggestions. The pairs for each laboratory session will be announced on the Moodle page.


### Pre-Lab and Post-Test

Each laboratory experience contains a section called *Pre-Lab*. In this section you will find the objectives for the experience and the concepts you should review and study before arriving at the laboratory. There will be a link on the Moodle page for a short quiz that you should take during the first 10 minutes of the laboratory session. The quiz will not be available after the 10 minutes have passed from the time the laboratory session begins. The concepts that you should review for the laboratory experience are concepts covered during the course conference sections. The short quiz will also have reading comprehension questions for the laboratory instructions.

The *Post-Test* is a short test about the same skills and concepts that are evaluated in the Pre-Lab. The Post-Test will be available on Moodle from the Friday of the laboratory session, until 11:59pm on the following Tuesday. These tests allows us to evaluate the efficacy of the laboratory experience in the learning of concepts and skills throughout the course. The points obtained will be used as a bonus for the laboratory grade.


### Turning in the work

When each laboratory experience is over, each pair should hand in the files with the implemented code or some other material. These deliveries will be done using Moodle and the instructions will be included in each laboratory experience.


---

---

## Services and programs we will use:

### Electronic book

The instructions for the laboratory experience for this semester are contained in the electronic book you can access in http://eip.ccom.uprrp.edu/_book-en/.

In the Moodle page we will indicate the name of the corresponding laboratory for each week and you can access the instructions using the index of the electronic book.

This is a preliminary version of the book and therefore may contain errors. We would appreciate it if you would tell us if you find any errors or if you have any suggestion that could help clarify the instructions.

### Bitbucket and Git

*Bitbucket* is a repository or deposit for digital files which can be accessed online and allows the development of group projects in an ordered and simple manner. The files for the Introduction to Programming Laboratory will be stored in this site and can be downloaded to your personal computers using *Git*.

*Git* is an open source program used to manage program files developed in groups. You can obtain this program in <http://git-scm.com/>. At the start of each laboratory experience you will download the necessary files to one of the classroom laptops using the command `git clone url` from the terminal in each computer.

### Terminal and Linux

To use Git in OS X we should use command lines in the *terminal*. The commands we will use are commands for the *Unix* operating system. Unix distinguishes between uppercase and lowercase letters, so be careful when writing the commands. A couple of basic Unix commands are:

---

|    **Command**     |                      **Action**                     |
| ------------------ | --------------------------------------------------- |
| ls                 | shows a list of the files in the directory      |
| mv name1 name2 | moves the contents of name1 to file name2 (changes the name of the file)       |
| cp name1 name2 | copies the contents of name1 to file name2       |
| rm name          | deletes file                                    |
| mkdir name       | creates a directory name within the current directory |
| cd ..              | moves to the previous directory                      |
| cd ~               | moves to the home directory                        |
| cd name          | moves to the name directory (within the current one)    |
| upper arrow | repeats the previous command                          |

           
**Figure 1.** Basic Unix commands.

---


### Qt

Qt is a programming application that is used by developers that use the C++ programming language. This environment works in, and can create versions of, applications for different platforms (desktop, mobile platforms, among others). Qt contains an integrated development environment (IDE), called *Qt Creator*. Within this environment, graphical interfaces can be created and programmed using the design option contained in Qt. We urge you to install Qt on your personal computer and explore the options this application provides.


### Virtual Machine

Each laptop in the laboratory has a *virtual machine* installed that contains the necessary Qt application to work with the laboratory experiences and a terminal where you can write the Unix commands to download the files.

You can install this virtual machine in your personal computer from the following link: http://eip.ccom.uprrp.edu/vms/eip-ubuntu-qt.tar.gz .

You should use this machine since it contains the version that allows each of the programs in the laboratory experience to work correctly. If you install another version of Qt, you may run into problems with some of the programs.


## How to obtain Qt

If you want to install the Qt application on your machine, you can go to the Qt project page, http://qt-project.org/. There, you will find more information about Qt, and you can download the program. You can watch videos on how to install Qt on different platforms in: <http://youtu.be/6eY5VSPYZCw> (Linux), <http://youtu.be/CImDCSxi7Wc> (Windows), <http://youtu.be/_zq-pSw3Ox4> (Mac).

---

---

## Using Qt

Qt is an application used by professional developers that has many possibilities and options. In each laboratory experience we will see how to use the basic options that we will need during the semester.

For the laboratory, we will mostly use the part for editing programs in C++, but there is also an option to design graphical interfaces. This option was used to incorporate the code that presents the graphical interfaces for the laboratory experiences. Learning to use this option is not part of this course, but you can learn to use it on your own. In https://docs.google.com/file/d/0B_6PcmpWnkFBOXJxVDlUNEVfOFk/edit you can find a presentation, prepared by the student Jonathan VÃ©lez, that shows basic aspects on how to use the option for designing graphical interfaces. 


### Projects in C++

Each C++ project is composed of various types of files. In Qt you will have files of type *source, header, and form*.

- **Source files:** These files have a `.cpp` extension (C plus plus) and contain the C++ code for your program. Among these files you will find `main.cpp`; this is the file the preprocessor will look for and its where your program starts. Another source file you will find in projects created with Qt is the `mainwindow.cpp` file; this file is created by Qt and contains the code associated with the main window designed with the design option (for example the functions that appear below "Private slots").

- **Header files:** These files have a `.h` extension and contain declarations of the functions that are used in the program. During the pre-processing of each program, the instruction `#include<name.h>` includes the contents of the file called "name.h" in the code for the `.cpp` file that contains this instruction.

- **Form files:** These files have a `.ui` extension (user interface) and contain the forms created with the design option. A file you will find in projects created with Qt is the `mainwindow.ui` file; this file is created by Qt and contains the design for the main program window.


---

---


## Laboratory session

In today's laboratory experience you will practice the use of the some of the programs you will use during the semester. You will use the programs that are installed in the virtual machine.


### Exercise 0: Create a directory for the laboratory files

Use the terminal and the command `mkdir` to create a directory `Documents/eip` for the laboratory files.


### Exercise 1: Starting your new project


#### Instructions

1. To start a C++ project, press the `New Project` button or go to the Qt main menu and in `File` select `New File or Project`. A window similar to the one in Figure 2 will appear. Select `Non-Qt Project`, `Plain C++ Project` and press `Choose`.

    ---

    ![figure2.png](images/figure2.png)

    **Figure 2.** Start C++ project without graphical applications.

    ---

2. Write the name of the project, select the directory where you would like to save it, press `Next` in that window and the following one, and then `Finish` in the next one.  
    
    This process will create a new Qt project with the skeleton for a basic C++ program that only displays "Hello World!". Before continuing, select `Projects` in the vertical menu to the left. A window called `Build Settings` should appear. In this window, make sure the `Shadow build` checkbox is NOT selected, as shown in Figure 3.
    
    ---

    ![figure3.png](images/figure3.png)

    **Figure 3.** The `Shadow build` option is not selected.
    
    ---

3. Return to the window where you can edit the program by selecting `Edit` in the left menu and double clicking `Sources` and then `main.cpp`. Press the green arrow on the left menu to execute the program. The results for the program will be displayed in the terminal window. If there were any errors, these would appear in the `Issues` window in Qt.

4. Change the contents of `main.cpp` to the following:

    ```cpp
    #include <iostream>
    using namespace std;
        
    int main()
    {
        cout << endl << "I like the programming laboratory." << endl;
        return 0;
    }

    ```
    ---

1. Press the green button in the left menu to compile and execute the program. A window will appear that offers the option to store the changes. Press `Save all`. When executed, if you did not commit any errors, the program will display "I like the programming laboratory." in the terminal window.


### Exercise 2: Download projects from Bitbucket

The files for this laboratory experience are stored in Bitbucket. In each laboratory session, you will download the Bitbucket folder that contains the files for that laboratory experience and you will save them in the `Documents/eip` directory you created in Exercise 0. To download the corresponding folder for this laboratory experience, open the terminal, use the Linux commands to move to the `Documents/eip` directory and write the command `git clone https://bitbucket.org/eip-uprrp/intro-introduction.git`. Go to the `Documents/eip` directory and verify that it contains the `intro-introduction` folder.


### Exercise 3: Open project already created, compile and run

In this exercise you will practice how to compile, fix errors, and execute a program using Qt.


#### Instructions

1. First delete the files created by Qt and close the files from the previous project. To do this, in the Qt main menu go to `Build` and select `Clean all`; then go to `File` and select `Close all projects and editors`.

2. Load the `QtCreator` project called `Practice` by double clicking the file `Practice.pro` in the `Documents/eip/introduction` directory on your computer. In the window that appears, press `Configure Project`.

    Each time you load or start a new project, make sure that `Shadow build` is not selected: in the menu to the left, select `Projects`, then in `Build Settings` verify that the checkbox for `Shadow build` is not selected, as we saw in Figure 3.

3. As you saw previously, Qt allows you to compile and execute a program by pressing the green arrow that appears in the left column. Press the arrow and notice that you obtain a window of "Issues" that occurred when compiling. The list that appears will show you information that allows you to find and fix the errors.

4. Select the `main.cpp` file in the `Sources` directory so you can find and fix the errors.

    Fix all of the errors and press the green arrow once again to compile and execute the program. Once you fix the errors, the program should open a window `Application Output` and display `Exit: 1`.

5. As mentioned earlier, during the compilation and execution process, Qt creates various files we should delete after terminating the program. To do this, in the `Build` option of the Qt menu, select `Clean All`.


### Exercise 4: Deliverables

During each laboratory experience, each pair will hand in some of the results of their work. These deliveries will be done in the "Deliverables" section that appears on Moodle. Today each student will practice handing in their work individually.


#### Instructions

1. Open the "Deliverables" link on Moodle and hand in the `main.cpp` file. Remember to use good programming techniques, including the name of the programmers as a comment at the beginning of the program.
